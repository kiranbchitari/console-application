﻿using System;

namespace Sample_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 1 && (args[0].Contains("?") || args[0].Contains("help") || args[0].Contains("-help")))
            {
                Usage();
                return;
            }

            try
            {
                Run(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}", ex.ToString());
            }
            Console.WriteLine("Bye from Data Transfer DW");
            Console.WriteLine("Bye");
        }

        static void Usage()
        {
            Console.WriteLine("Usage: DataTransferDW.exe [-CheckOnly | /CheckOnly] [/Force | -Force]");
            Console.WriteLine("  Will do a series of checks of Imported and processed ProductSku data");
            Console.WriteLine("   and if it turns out to be of good quality, will initiate data transfer to FlexOffer Data Warehouse");
            Console.WriteLine("  Use checkOnly option to just test the quality and not do actual data transfer");
            Console.WriteLine("  Use Force option to anyway start the data transfer to FlexOffers Data Warehouse");
        }

        static void Run(string[] args)
        {
            Console.WriteLine("DataTransferDW", "");

            Console.WriteLine("Welcome to Data Tranfer to DW");
            Console.WriteLine("Data Transfer DW Begins");

            bool bForce;
            bool bCheckOnly;
            ParseCommandLineArgs(args, out bForce, out bCheckOnly);
            DataManager manager = new DataManager();
            manager.Run(bForce, bCheckOnly);
        }

        static void ParseCommandLineArgs(string[] args, out bool bForce, out bool bCheckOnly)
        {
            bForce = bCheckOnly = false;
            string arg;

            for (int i = 0; i < args.Length; i++)
            {
                arg = args[i].ToLower();
                if (arg == "-force" || arg == "/force")
                    bForce = true;
                else if (arg == "-checkonly" || arg == "/checkonly")
                    bCheckOnly = true;
            }
        }
    }
}
